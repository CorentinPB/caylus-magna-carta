from enum import Enum, unique
from caylusMC_phases import Phase

def ordinal_number(n: int) -> str:
    """Get the ordinal number."""
    if n <= 0:
        raise Exception('The ordinal number is not defined for non-positive integers.')
    else:
        digit = n % 10  # type: int
        letter_suffix = None  # type: str
        if digit == 1:
            letter_suffix = 'st'
        elif digit == 2:
            letter_suffix = 'nd'
        elif digit == 3:
            letter_suffix = 'rd'
        else:
            letter_suffix = 'th'
        return str(n) + letter_suffix


def indent(n_indent: int) -> str:
    """Get a string in order to create an indentation."""
    return '  ' * n_indent

class Castle:
    """Castle is composed of 3 parts: dungeon, walls, towers."""
    """
    Prestige point tokens (for the 3 parts of the Castle) are sorted out according to their value and placed next to
    the Castle. In three player games, 1 token of each value is removed; in two player games, 2 tokens of each value are
    removed.
    """

    def __init__(self, front_color: str, name: str, n_castle_tokens, n_prestige_pts: int):
        """Initialization of a part (dungeon, walls, towers) of the castle."""
        # Attributes obtained from the XML file.
        self.front_color = front_color  # type: str
        self.name = name  # type: str
        self.n_castle_tokens = n_castle_tokens  # type: Array[int]
        self.n_prestige_pts = n_prestige_pts  # type: int
        # Attributes to play a game.
        self.current_n_castle_tokens = None  # type: int

    def setup(self, n_players: int) -> None:
        """Setup the part (dungeon, walls, towers) of the castle."""
        self.current_n_castle_tokens = self.n_castle_tokens[n_players]  # type: int

class Effect:
    """Effect (primary or secondary) of a building."""

    def __init__(self, text: str, phase: Phase, money_resources_cost=None, money_resources_gain=None):
        """Initialization of an effect of a building."""
        self.text = text  # type: str
        self.phase = phase  # type: Phase
        self.money_resources_cost = money_resources_cost  # unused
        self.money_resources_gain = money_resources_gain  # type: Tuple[MoneyResource, int]

@unique
class Location(Enum):
    """Enumeration of all the possible locations of the player buildings."""
    HAND = 0
    PILE = 1
    DISCARD = 2
    ROAD = 3
    REPLACED = 4

@unique
class Action(Enum):
    """Enumeration of all the possible actions of the phase Actions."""
    PICK_CARD = ('Pick a card')
    REPLACE_CARDS_IN_HAND = ('Replace all the cards in your hand')
    PLACE_WORKER_ON_BUILDING = ('Place a worker on a building')
    CONSTRUCT_BUILDING_FROM_HAND = ('Construct a building from your hand')
    CONSTRUCT_PRESTIGE_BUILDING_BEGINNER = ('Construct a prestige building (Beginner version)')
    CONSTRUCT_PRESTIGE_BUILDING_STANDARD = ('Construct a prestige building (Standard version)')
    PASSING = ('Passing')

    def __init__(self, txt: str):
        """Initialization of an action."""
        self.txt = txt  # Type: str

class Version:
    """All 2 versions of the game: beginner and standard."""

    def __init__(self, name: str):
        """Initialization of a version of the game."""
        # Attributes obtained from the XML file.
        self.name = name  # type: str

    def is_beginner(self) -> bool:
        """Indicates if it is the beginner version; it is the standard version otherwise."""
        return self.name.lower() == 'beginner'


class ColorPlayer:
    """All 4 colors of the players: red, green, orange and blue."""

    colors_players = {}  # type: Dict[str, ColorPlayer] # All colors of players (indexed by their names).

    def __init__(self, name: str, background_player_building=None):
        """Initialization of a color of a player."""
        # Attributes obtained from the XML file.
        self.name = name  # type: str
        self.background_player_building = background_player_building  # type: BackgroundPlayerBuilding
        ColorPlayer.colors_players[name] = self
        # Attributes to play a game.
        self.player = None  # type: Player

    def setup(self, background_player_building) -> None:
        """Setup the background of a player building."""
        self.background_player_building = background_player_building