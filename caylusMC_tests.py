import unittest
from caylusMC_utils import ordinal_number
from caylusMC_game import Game, GameElement

"""Test cases of the module 'caylusMC_utils'."""
class TestsUtils(unittest.TestCase):

    """Test function 'ordinal_number' for the value 1."""
    def test_ordinal_1(self):
        ordinal = ordinal_number(1)
        # Checks that 'ordinal' equals 'st'
        self.assertEqual(ordinal, "1st")

    """Test function 'ordinal_number' for the value 2."""
    def test_ordinal_2(self):
        ordinal = ordinal_number(2)
        # Checks that 'ordinal' equals 'st'
        self.assertEqual(ordinal, "2nd")

    """Test function 'ordinal_number' for the value 3."""
    def test_ordinal_3(self):
        ordinal = ordinal_number(3)
        # Checks that 'ordinal' equals 'st'
        self.assertEqual(ordinal, "3rd")

    """Test function 'ordinal_number' for the value 4."""
    def test_ordinal_4(self):
        ordinal = ordinal_number(4)
        # Checks that 'ordinal' equals 'st'
        self.assertEqual(ordinal, "4th")

    """Test function 'ordinal_number' for the value 5."""
    def test_ordinal_574(self):
        ordinal = ordinal_number(574)
        # Checks that 'ordinal' equals 'st'
        self.assertEqual(ordinal, "574th")

    """Test function 'ordinal_number' for the value -1."""
    def test_ordinal_minus_1(self):
        # Checks that 'ordinal' raises an exception
        try:
            ordinal = ordinal_number(-1)
            passed = True
        except Exception:
            passed = False
        self.assertFalse(passed)

"""Test cases of the module 'caylusMC_game'."""
class TestsGame(unittest.TestCase):

    """Test function 'ordinal_number' for the value 1."""
    def test_remove_tokens_castle_when_nb_is_zero(self):
        print("f")

"""Test cases of the module 'caylusMC_players'."""
class TestPlayer(unittest.TestCase):

    # E.g.: current_money_resources = 3F,2W,3S,3G and resource_costs = -1F,-3W,(S),-1G requires resource_payments = -1F,-2W,-0S,-1G-1G.
    """Test function 'resource_all_payments' for the costs 1F, 3W, 1G."""
    def test_resource_all_payments_3F_2W_3S_3G_costs_1F_3W_1G(self):
        print("f")

    # E.g.: current_money_resources = 3F,2W,3S,3G and resource_costs = -1any,(F),-1W,(S),(G) requires resource_payments = -1F,-1W,-0S,-0G or -0F,-2W,-0S,-0G or -0F,-1W,-1S,-0G (but not -0F,-1W,-0S,-1G).
    """Test function 'resource_all_payments' for the costs 1F, 3W, 1G."""
    def test_resource_all_payments_3F_2W_3S_3G_costs_1A_1W(self):
        print("f")
        
        
